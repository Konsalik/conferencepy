# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SelectContacts.ui'
#
# Created: Fri Sep  6 13:01:19 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_SetupCallForm(object):
    def setupUi(self, SetupCallForm):
        SetupCallForm.setObjectName("SetupCallForm")
        SetupCallForm.resize(1280, 720)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(SetupCallForm.sizePolicy().hasHeightForWidth())
        SetupCallForm.setSizePolicy(sizePolicy)
        self.contactList = QtGui.QListWidget(SetupCallForm)
        self.contactList.setGeometry(QtCore.QRect(230, 130, 821, 451))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.contactList.setFont(font)
        self.contactList.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.contactList.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.contactList.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.contactList.setObjectName("contactList")
        self.contactBox = QtGui.QComboBox(SetupCallForm)
        self.contactBox.setGeometry(QtCore.QRect(230, 60, 671, 51))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.contactBox.setFont(font)
        self.contactBox.setObjectName("contactBox")
        self.btnAdd = QtGui.QPushButton(SetupCallForm)
        self.btnAdd.setGeometry(QtCore.QRect(920, 60, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.btnAdd.setFont(font)
        self.btnAdd.setObjectName("btnAdd")
        self.btnBack = QtGui.QPushButton(SetupCallForm)
        self.btnBack.setGeometry(QtCore.QRect(230, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName("btnBack")
        self.btnStart = QtGui.QPushButton(SetupCallForm)
        self.btnStart.setGeometry(QtCore.QRect(650, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnStart.setFont(font)
        self.btnStart.setObjectName("btnStart")
        self.btnClose = QtGui.QPushButton(SetupCallForm)
        self.btnClose.setGeometry(QtCore.QRect(1230, 0, 51, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btnClose.setFont(font)
        self.btnClose.setObjectName("btnClose")

        self.retranslateUi(SetupCallForm)
        self.contactBox.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(SetupCallForm)

    def retranslateUi(self, SetupCallForm):
        SetupCallForm.setWindowTitle(QtGui.QApplication.translate("SetupCallForm", "Select Contacts", None, QtGui.QApplication.UnicodeUTF8))
        self.btnAdd.setText(QtGui.QApplication.translate("SetupCallForm", "Add", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("SetupCallForm", "Back", None, QtGui.QApplication.UnicodeUTF8))
        self.btnStart.setText(QtGui.QApplication.translate("SetupCallForm", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.btnClose.setText(QtGui.QApplication.translate("SetupCallForm", "X", None, QtGui.QApplication.UnicodeUTF8))

