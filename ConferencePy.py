'''
Created on 30 Aug 2013

@author: gerrit
'''
import sys
from PySide import QtGui, QtCore

import pystrix3 as pystrix
from commands import *
import queue
import ami_events
import MainWindow
from ContactManager import ContactManager
from ContactSelector import ContactSelector
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import contacts

class ConferencePy(QtGui.QMainWindow, MainWindow.Ui_MainWindow):
    
    def __init__(self, parent=None):
        super(ConferencePy, self).__init__(parent)
        
        self.setupUi(self)
        
        self.btnClose.clicked.connect(self.btnCloseClicked)
        self.btnManageContacts.clicked.connect(self.btnManageContactsClicked)
        self.btnNewConference.clicked.connect(self.btnNewConferenceClicked)
                
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.showFullScreen()
        
        #http://docs.sqlalchemy.org/en/rel_0_7/orm/tutorial.html
        engine = create_engine('sqlite:///contacts', echo=True)
        contacts.Base.metadata.create_all(engine)
        self.Session = sessionmaker()
        self.Session.configure(bind=engine)
        
    '''def initUI(self):

        #queue to send commands from main application
        self.cmdQueue = queue.Queue()
        #queue to send status from amithread
        self.mainQueue = queue.Queue()

        self.amit = AMIThread(self.mainQueue,self.cmdQueue)
        self.amit.setDaemon(True)

        self.qleNumber = QtGui.QLineEdit(self)
        self.qleNumber.move(150, 10)

        btnConn = QtGui.QPushButton("Connect to AMI", self)
        btnConn.move(30, 50)

        self.btnDial = QtGui.QPushButton("Dial In", self)
        self.btnDial.move(150, 50)
      
        btnConn.clicked.connect(self.btnConnClicked)            
        self.btnDial.clicked.connect(self.btnDialClicked)
        self.btnDial.setDisabled(True)
        
        self.statusBar()
        
        self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('AMI GUI Test')
        self.show()'''
    def btnCloseClicked(self):
        self.close()
        
    def btnManageContactsClicked(self):
        """
        Nothing here... yet
        """
        #self.addWindow(ContactManager)
        CM = ContactManager(session = self.Session)
        CM.exec_()
    
    def btnNewConferenceClicked(self):
        CS = ContactSelector(session = self.Session)
        CS.exec_()
    
    def btnConnClicked(self):
        '''self.amit.start()
        if self.mainQueue.get() == ami_events.ConnectionSuccess:
            self.statusBar().showMessage("Connection Succesfull")
            self.btnDial.setEnabled(True)'''

    def btnDialClicked(self):
        '''self.statusBar().showMessage("Dialing: "+self.qleNumber.text()+"...")

        request = pystrix.ami.core.Originate_Application(channel="SIP/"+self.qleNumber.text()+"@10.10.11.24", application="ConfBridge", data=["001"])
        self.cmdQueue.put(request)'''
       

def main():
    
    app = QtGui.QApplication(sys.argv)
    MainForm = ConferencePy()
    MainForm.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()