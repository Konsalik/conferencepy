'''
Created on 05 Sep 2013

@author: gerrit
'''

from contacts import Contact
from PySide import QtGui, QtCore
import ManageContacts
from ContactDialog import ContactDialog

class ContactManager(QtGui.QDialog, ManageContacts.Ui_ManageContactsForm):


    def __init__(self, session, parent=None):
        '''
        Constructor
        '''
        super(ContactManager, self).__init__(parent)
        
        self.Session = session
        
        self.setupUi(self)
        
        self.btnClose.clicked.connect(self.btnCloseClicked)
        self.btnBack.clicked.connect(self.btnCloseClicked)
        self.btnNew.clicked.connect(self.newContact)
        
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.showFullScreen()
        
        self.loadContacts()
        
    def btnCloseClicked(self):
        self.close()
        
    #http://stackoverflow.com/questions/10610804/design-of-the-model-for-qtableview-in-pyside-sqlalchemy
    def loadContacts(self):
        session = self.Session()
        rows = []
        for contact in session.query(Contact).order_by(Contact.name):
            rows.append((contact.id, contact.name, contact.number, "Edit", "Delete"))
                 
        self.ContactsView.clear()
        
        self.ContactsView.setColumnCount(4)
        self.ContactsView.setHorizontalHeaderLabels(("Name", "Number","",""))
        
        self.ContactsView.setColumnWidth(3,60)
        self.ContactsView.setColumnWidth(4,60)
        
        #http://stackoverflow.com/questions/11839813/how-to-make-qtablewidgets-columns-assume-the-maximum-space
        '''header = self.ContactsView.horizontalHeader()
        header.setStretchLastSection(True)'''
        
        self.ContactsView.setRowCount(len(rows))
        
        #http://pysnippet.blogspot.com/2010/06/qsignalmapper-at-your-service.html
        editMapper = QtCore.QSignalMapper(self)
        deleteMapper = QtCore.QSignalMapper(self)
                
        for row, cols in enumerate(rows):
            for col, text in enumerate(cols):
                #print("%i %s" % (col, text))
                if col == 0:
                    cid = int(text)              
                
                elif col == 3: 
                    '''Edit'''
                    editButton = QtGui.QPushButton()
                    editButton.setText("E")
                    self.ContactsView.setCellWidget(row, col-1, editButton)
                    
                    editMapper.setMapping(editButton, cid)
                    editButton.clicked.connect(editMapper.map)
                  
                elif col == 4:
                    '''Delete'''  
                    deleteButton = QtGui.QPushButton()
                    deleteButton.setText("X")
                    self.ContactsView.setCellWidget(row, col-1, deleteButton)
                    
                    deleteMapper.setMapping(deleteButton, cid)
                    deleteButton.clicked.connect(deleteMapper.map)
                    
                else:
                    item = QtGui.QTableWidgetItem(text)
                    self.ContactsView.setItem(row, col-1, item)
                # Optional, but very useful.
                #table_item.setData(QtCore.Qt.UserRole+1, user)
        
        editMapper.mapped.connect(self.editContact)
        deleteMapper.mapped.connect(self.deleteContact)
        
        self.ContactsView.resizeColumnsToContents()
        self.ContactsView.resizeRowsToContents()
        
        self.ContactsView.setColumnWidth(2,60)
        self.ContactsView.setColumnWidth(3,60)
        
        session.close()
    
    def editContact(self, cid):
        session = self.Session()
        
        oldContact = session.query(Contact).get(cid)
        
        CD = ContactDialog(contact = oldContact)
        CD.exec_()
        
        retContact = CD.getContact()
        
        if retContact != None:
        
            oldContact.name = retContact.name
            oldContact.number = retContact.number
            
            session.commit()
        session.close()
        
        self.loadContacts()
        
    def newContact(self):
        CD = ContactDialog()
        CD.exec_()
        
        contact = CD.getContact()
        
        if contact:
        
            session = self.Session()
            session.add(contact)
            
            session.commit()
            session.close()
        
            self.loadContacts()
        
    def deleteContact(self, cid):
        session = self.Session()
        
        contact = session.query(Contact).get(cid)
        session.delete(contact)
        
        session.commit()
        session.close()
        
        self.loadContacts()