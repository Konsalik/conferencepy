# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AddContact.ui'
#
# Created: Thu Sep  5 23:41:35 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AddContactForm(object):
    def setupUi(self, AddContactForm):
        AddContactForm.setObjectName("AddContactForm")
        AddContactForm.resize(1280, 720)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AddContactForm.sizePolicy().hasHeightForWidth())
        AddContactForm.setSizePolicy(sizePolicy)
        self.btnBack = QtGui.QPushButton(AddContactForm)
        self.btnBack.setGeometry(QtCore.QRect(230, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName("btnBack")
        self.edtName = QtGui.QLineEdit(AddContactForm)
        self.edtName.setGeometry(QtCore.QRect(440, 210, 621, 71))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.edtName.setFont(font)
        self.edtName.setObjectName("edtName")
        self.label = QtGui.QLabel(AddContactForm)
        self.label.setGeometry(QtCore.QRect(280, 220, 151, 51))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.edtNumber = QtGui.QLineEdit(AddContactForm)
        self.edtNumber.setGeometry(QtCore.QRect(440, 290, 621, 71))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.edtNumber.setFont(font)
        self.edtNumber.setObjectName("edtNumber")
        self.label_2 = QtGui.QLabel(AddContactForm)
        self.label_2.setGeometry(QtCore.QRect(230, 300, 201, 51))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.btnSave = QtGui.QPushButton(AddContactForm)
        self.btnSave.setGeometry(QtCore.QRect(650, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnSave.setFont(font)
        self.btnSave.setObjectName("btnSave")
        self.btnClose = QtGui.QPushButton(AddContactForm)
        self.btnClose.setGeometry(QtCore.QRect(1230, 0, 51, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btnClose.setFont(font)
        self.btnClose.setObjectName("btnClose")

        self.retranslateUi(AddContactForm)
        QtCore.QMetaObject.connectSlotsByName(AddContactForm)

    def retranslateUi(self, AddContactForm):
        AddContactForm.setWindowTitle(QtGui.QApplication.translate("AddContactForm", "Add Contact", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("AddContactForm", "Back", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("AddContactForm", "Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("AddContactForm", "Number", None, QtGui.QApplication.UnicodeUTF8))
        self.btnSave.setText(QtGui.QApplication.translate("AddContactForm", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.btnClose.setText(QtGui.QApplication.translate("AddContactForm", "X", None, QtGui.QApplication.UnicodeUTF8))

