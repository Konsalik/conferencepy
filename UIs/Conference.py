# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Conference.ui'
#
# Created: Mon Sep 23 22:34:28 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1280, 720)
        self.contactsView = QtGui.QTableWidget(Form)
        self.contactsView.setGeometry(QtCore.QRect(230, 51, 821, 531))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.contactsView.setFont(font)
        self.contactsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.contactsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.contactsView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.contactsView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.contactsView.setShowGrid(False)
        self.contactsView.setObjectName("contactsView")
        self.contactsView.setColumnCount(0)
        self.contactsView.setRowCount(0)
        self.btnEnd = QtGui.QPushButton(Form)
        self.btnEnd.setGeometry(QtCore.QRect(229, 600, 821, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnEnd.setFont(font)
        self.btnEnd.setObjectName("btnEnd")
        self.btnClose = QtGui.QPushButton(Form)
        self.btnClose.setGeometry(QtCore.QRect(1230, 0, 51, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btnClose.setFont(font)
        self.btnClose.setObjectName("btnClose")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Conference", None, QtGui.QApplication.UnicodeUTF8))
        self.contactsView.setSortingEnabled(True)
        self.btnEnd.setText(QtGui.QApplication.translate("Form", "End Conference", None, QtGui.QApplication.UnicodeUTF8))
        self.btnClose.setText(QtGui.QApplication.translate("Form", "X", None, QtGui.QApplication.UnicodeUTF8))

