# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ManageContacts.ui'
#
# Created: Thu Sep  5 22:56:24 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_ManageContactsForm(object):
    def setupUi(self, ManageContactsForm):
        ManageContactsForm.setObjectName("ManageContactsForm")
        ManageContactsForm.resize(1280, 720)
        self.btnBack = QtGui.QPushButton(ManageContactsForm)
        self.btnBack.setGeometry(QtCore.QRect(230, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName("btnBack")
        self.btnNew = QtGui.QPushButton(ManageContactsForm)
        self.btnNew.setGeometry(QtCore.QRect(650, 600, 400, 81))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.btnNew.setFont(font)
        self.btnNew.setObjectName("btnNew")
        self.ContactsView = QtGui.QTableWidget(ManageContactsForm)
        self.ContactsView.setGeometry(QtCore.QRect(230, 51, 821, 531))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.ContactsView.setFont(font)
        self.ContactsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ContactsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.ContactsView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.ContactsView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.ContactsView.setShowGrid(False)
        self.ContactsView.setObjectName("ContactsView")
        self.ContactsView.setColumnCount(0)
        self.ContactsView.setRowCount(0)
        self.btnClose = QtGui.QPushButton(ManageContactsForm)
        self.btnClose.setGeometry(QtCore.QRect(1230, 0, 51, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btnClose.setFont(font)
        self.btnClose.setObjectName("btnClose")

        self.retranslateUi(ManageContactsForm)
        QtCore.QMetaObject.connectSlotsByName(ManageContactsForm)

    def retranslateUi(self, ManageContactsForm):
        ManageContactsForm.setWindowTitle(QtGui.QApplication.translate("ManageContactsForm", "Manage Contacts", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("ManageContactsForm", "Back", None, QtGui.QApplication.UnicodeUTF8))
        self.btnNew.setText(QtGui.QApplication.translate("ManageContactsForm", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.ContactsView.setSortingEnabled(True)
        self.btnClose.setText(QtGui.QApplication.translate("ManageContactsForm", "X", None, QtGui.QApplication.UnicodeUTF8))

