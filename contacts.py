'''
Created on 05 Sep 2013

@author: gerrit
'''
#http://docs.sqlalchemy.org/en/rel_0_7/orm/tutorial.html

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean

Base = declarative_base()

class Contact(Base):
    __tablename__ = 'contacts'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    number = Column(String)
    #type = Column(Boolean)

    def __init__(self, name, number):
        self.name = name
        self.number = number

    def __repr__(self):
        return "<User('%s','%s')>" % (self.name, self.number)