import queue
import pystrix3 as pystrix
import ami_events
from time import sleep

from PySide import QtCore

_HOST = '10.10.11.98'
_USERNAME = 'cpi'
_PASSWORD = 'cpisecurepassword'

class AMICore(QtCore.QObject):
    
    _manager = None #The AMI conduit for communicating with the local Asterisk server
    _kill_flag = False #True when the core has shut down of its own accord
    
    userSpeakStateChange = QtCore.Signal(str, bool)
    newState = QtCore.Signal(str, str)
    
    

    def __init__(self, queue, parent = None):
        QtCore.QObject.__init__(self, parent)

        self.cmdQueue = queue
        self._manager = pystrix.ami.Manager()
        #self._manager._debug =True;

        self._register_callbacks()

        try:

            self._manager.connect(_HOST)       
            action = pystrix.ami.core.Login(_USERNAME, _PASSWORD)
            self._manager.send_action(action)

        except pystrix.ami.ManagerSocketError as e:
            self._kill_flag = True
            raise ConnectionError("Unable to connect to Asterisk server: %(error)s" % {
             'error': str(e),
            })
        except pystrix.ami.core.ManagerAuthError as reason:
            self._kill_flag = True
            raise ConnectionError("Unable to authenticate to Asterisk server: %(reason)s" % {
             'reason': reason,
            })
        except pystrix.ami.ManagerError as reason:
            self._kill_flag = True
            raise ConnectionError("An unexpected Asterisk error occurred: %(reason)s" % {
             'reason': reason,
            })

        self._manager.monitor_connection()
    
    @QtCore.Slot()  
    def doWork(self):
        while self.is_alive():
            #pew = self.cmdQueue.get()
            #f, args, kwargs = self.cmdQueue.get()
            #kwargs["manager"] = ami_manager
            #sleep(0)
            #f(*args, **kwargs)
            req = self.cmdQueue.get()
            print("Request sent: " + str(self._manager.send_action(req)))
            self.cmdQueue.task_done()
        self.kill()
    
    def _userSpeakStateChange(self, event, manager):
        if event['Conference'] == '100':
            ts = event["TalkingStatus"]
            if ts == 'on':
                tsb = True
            else:
                tsb = False
            self.userSpeakStateChange.emit(event["Channel"], tsb)
            
    def _newState(self, event, manager):
        #print(event)
        self.newState.emit(event["Channel"], event['CallerIDNum'])
        
    def logger(self, event, manager):
        print(event)
        
        
    def _register_callbacks(self):
        #This sets up some event callbacks, so that interesting things, like calls being
        #established or torn down, will be processed by your application's logic. Of course,
        #since this is just an example, the same event will be registered using two different
        #methods.

        #The event that will be registered is 'FullyBooted', sent by Asterisk immediately after
        #connecting, to indicate that everything is online. What the following code does is
        #register two different callback-handlers for this event using two different
        #match-methods: string comparison and class-match. String-matching and class-resolution
        #are equal in performance, so choose whichever you think looks better.
        
        #self._manager.register_callback('FullyBooted', self._handle_string_event)
        self._manager.register_callback(pystrix.ami.app_confbridge_events.ConfbridgeTalking, self._userSpeakStateChange)
        self._manager.register_callback(pystrix.ami.core_events.Newstate, self._newState)
        self._manager.register_callback(pystrix.ami.app_confbridge_events.ConfbridgeList, self.logger)
        #Now, when 'FullyBooted' is received, both handlers will be invoked in the order in
        #which they were registered.

        #A catch-all-handler can be set using the empty string as a qualifier, causing it to
        #receive every event emitted by Asterisk, which may be useful for debugging purposes.
        
        #self._manager.register_callback('', self.logger)

        #Additionally, an orphan-handler may be provided using the special qualifier None,
        #causing any responses not associated with a request to be received. This should only
        #apply to glitches in pre-production versions of Asterisk or requests that timed out
        #while waiting for a response, which is also indicative of glitchy behaviour. This
        #handler could be used to process the orphaned response in special cases, but is likely
        #best relegated to a logging role.
        self._manager.register_callback(None, self._handle_event)

        #And here's another example of a registered event, this time catching Asterisk's
        #Shutdown signal, emitted when the system is shutting down.
        
        self._manager.register_callback('Shutdown', self._handle_shutdown)

    def _handle_shutdown(self, event, manager):
        self._kill_flag = True

    def _handle_event(self, event, manager):
        '''print(event)'''

    def is_alive(self):
        return not self._kill_flag

    def kill(self):
        self._manager.close()

