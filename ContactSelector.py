'''
Created on 06 Sep 2013

@author: gerrit
'''
from PySide import QtGui, QtCore
import SelectContacts
from contacts import Contact
from ConferenceManager import ConferenceManager

class ContactSelector(QtGui.QDialog, SelectContacts.Ui_SetupCallForm):
    '''
    classdocs
    '''


    def __init__(self, session, parent=None):
        '''
        Constructor
        '''
        super(ContactSelector, self).__init__(parent)
        
        self.Session = session
        
        self.setupUi(self)
        
        self.btnClose.clicked.connect(self.btnCloseClicked)
        self.btnBack.clicked.connect(self.btnCloseClicked)
        self.btnAdd.clicked.connect(self.btnAddClicked)
        self.btnStart.clicked.connect(self.btnStartClicked)
        self.contactList.doubleClicked.connect(self.removeContact)
        
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.showFullScreen()
        
        self.loadContacts()
        
    def btnCloseClicked(self):
        self.close()
        
    def loadContacts(self):
        session = self.Session()
        
        self.contactBox.addItem("Select Contacts to Call")
        self.contactBox.insertSeparator(1)
        #self.contactBox.addItem("New Contact")
        #self.contactBox.insertSeparator(3)

        for contact in session.query(Contact).order_by(Contact.name):
            self.contactBox.addItem("%s (%s)" % (contact.name, contact.number), contact.id)
            
        session.close()
        
    def btnAddClicked(self):
        iid = self.contactBox.currentIndex()
        
        if iid < 2:
            return
        
        for c in range(self.contactList.count()):
            if self.contactList.item(c).data(QtCore.Qt.UserRole) == self.contactBox.itemData(iid):
                #item already added
                return
            
        item = QtGui.QListWidgetItem(self.contactBox.itemText(iid))
        item.setData(QtCore.Qt.UserRole,self.contactBox.itemData(iid))
        
        self.contactList.addItem(item)
        
    def btnStartClicked(self):
        pass
        ConMan = ConferenceManager(session = self.Session, contacts = self.retContacts())
        ConMan.exec_()
        
    def retContacts(self):
        contacts = []
        for c in range(self.contactList.count()):
            contacts.append(self.contactList.item(c).data(QtCore.Qt.UserRole))    
        return contacts
        
    def removeContact(self):
        self.contactList.takeItem(self.contactList.currentRow())