'''
Created on 26 Sep 2013

@author: gerrit
'''
import threading
import subprocess

import pexpect
from time import sleep

class linphoneWorker(threading.Thread):
    '''
    classdocs
    '''


    def __init__(self, startEvent, endEvent):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
        self.startEvent = startEvent
        self.endEvent = endEvent
        
    def run(self):
        self.lp = pexpect.spawn('linphonec', timeout=None)
        self. lp.logfile = open("/home/cpi/deploy/linphone_log.txt", "w")
        
        self.lp.expect("linphonec>")
        self.lp.sendline("")
        self.lp.expect("linphonec>")
        self.startEvent.wait()
        self.lp.sendline("call sip:100@10.10.11.98")
        self.lp.expect("linphonec>")
        self.lp.sendline("")
        self.lp.expect("linphonec>")
        self.endEvent.wait()
        self.lp.sendline("quit")
        sleep(10)
        