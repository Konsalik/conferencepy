'''
Created on 19 Sep 2013

@author: gerrit
'''
from contacts import Contact
from PySide import QtGui, QtCore

from ami import AMICore
from linphoneWorker import linphoneWorker

import Conference
import queue
import re
import threading

import pystrix3 as pystrix

class ConferenceManager(QtGui.QDialog, Conference.Ui_Form):

    contacts = []
    chanContact = {}
    rowContact = {}

    def __init__(self, session, contacts, parent=None):
        super(ConferenceManager, self).__init__(parent)
        
        self.Session = session
        self.contactIds = contacts
        
        self.setupUi(self)
        
        self.btnClose.clicked.connect(self.btnCloseClicked)
        self.btnEnd.clicked.connect(self.btnEndClicked)
        
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.showFullScreen()
        
        #queue to send commands from main application
        self.cmdQueue = queue.Queue()
        #queue to send status from amithread
        self.mainQueue = queue.Queue()
        
        self.ami = AMICore(self.cmdQueue)
    
        self.amiThread = QtCore.QThread()
        self.ami.moveToThread(self.amiThread)
        
        self.amiThread.started.connect(self.ami.doWork)
        self.ami.userSpeakStateChange.connect(self.userSpeakStateChange)
        self.ami.newState.connect(self.updateChanContact)
        
        self.start = threading.Event()
        self.stop = threading.Event()
        localClient = linphoneWorker(self.start, self.stop)
        localClient.setDaemon(True)
        
        localClient.start()
        
        self.runConference()
    
    def btnCloseClicked(self):
        self.close()
    
    def runConference(self):
        session = self.Session()
        
        self.contactsView.clear()
        
        self.chanContact = {}
        self.rowContact = {}
        
        self.contactsView.setColumnCount(3)
        self.contactsView.setHorizontalHeaderLabels(("Talking", "Contact Name","Action"))
        
        self.contactsView.setRowCount(len(self.contactIds)+1)
        
        actionMapper = QtCore.QSignalMapper(self)
        
        rows = []
        self.rowContact['001'] = 0
        
        item = QtGui.QTableWidgetItem("")
        self.contactsView.setItem(0, 0, item)
        
        item = QtGui.QTableWidgetItem("Self")
        self.contactsView.setItem(0, 1, item)
        
        for i, c in enumerate(self.contactIds):
            contact = session.query(Contact).get(c)
            rows.append([contact.id, contact.name, "Action"])
            self.contacts.append(contact)
            self.rowContact[re.sub('\+', '00', contact.number)] = i+1
            
        for row, cols in enumerate(rows):
            for col, text in enumerate(cols):
                if col == 0:
                    #cid = int(text)
                    item = QtGui.QTableWidgetItem("Not Talking")
                    self.contactsView.setItem(row+1, col, item)         
                    
                elif col == 1: 
                    item = QtGui.QTableWidgetItem(text)
                    self.contactsView.setItem(row+1, col, item)
                
                elif col == 2: 
                    '''Action'''
                    editButton = QtGui.QPushButton()
                    editButton.setText("Hang Up")
                    self.contactsView.setCellWidget(row+1, col, editButton)
                    
                    actionMapper.setMapping(editButton, row+1)
                    editButton.clicked.connect(actionMapper.map)
            
                # Optional, but very useful.
                #table_item.setData(QtCore.Qt.UserRole+1, user)
        self.contactsView.resizeColumnsToContents()
        self.contactsView.resizeRowsToContents()
        
        actionMapper.mapped.connect(self.participantAction)
        
        self.amiThread.start()
        
        for c in self.contacts:
            number = re.sub('\+', '00', c.number)
            
            #request = pystrix.ami.core.Originate_Application(channel="local/%s@from-internal"%(number) , application="ConfBridge", data=["100"])
            request = pystrix.ami.core.Originate_Context(channel="local/%s@from-internal"%(number), context='from-internal', extension = '100', priority= '1')
            self.cmdQueue.put(request)
        
        self.start.set()
        #request = pystrix.ami.core.Originate_Context(channel="local/0027824353506@from-internal", context='from-internal', extension = '100', priority= '1')
        #request = pystrix.ami.core.Originate_Application(channel="local/002", application="ConfBridge", data=["100"])
        #request = pystrix.ami.core.Originate_Application(channel="local/0027824353506@from-internal", callerid="piet<0027824353506>", application="ConfBridge", data=["100"])
        #self.cmdQueue.put(request)
    def participantAction(self, row):
        channel = self.getChannelRow(row)
        request = pystrix.ami.core.Hangup(channel)
        self.cmdQueue.put(request)
    
    def userSpeakStateChange(self, channel, onOff):
        #print("Channel %s is %s"%(channel, str(onOff)))
        #self.updateChanContact(channel)
        row = self.getContactRow(channel)
        
        if row > -1:
            if onOff:
                item = QtGui.QTableWidgetItem("Talking")
            else:
                item = QtGui.QTableWidgetItem("Not Talking")
            self.contactsView.setItem(row, 0, item)
        
    def updateChanContact(self, channel, number):
        self.chanContact.pop(channel, None)

        if channel.startswith('SIP'):
            if number in self.rowContact:
                self.chanContact[channel] = number
                
                print(self.chanContact)
            else:
                return
            
    '''def updateNewChanContact(self, channelOld, channelNew):
        if channelOld in self.chanContact:
            number = self.chanContact[channelOld]
            self.chanContact[channelNew] = number'''
    
    def getContactRow(self, channel):
        if channel in self.chanContact:
            return self.rowContact[self.chanContact[channel]]
        else:
            return -1
        
    def getChannelRow(self, row):
        _chanContact = {v:k for k, v in self.chanContact.items()}
        _rowContact = {v:k for k, v in self.rowContact.items()}
        
        if row in _rowContact:
            return _chanContact[_rowContact[row]]
        else:
            return -1
    
    def btnEndClicked(self):
        self.stop.set()
        self.close()
        