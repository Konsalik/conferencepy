'''
Created on 05 Sep 2013

@author: gerrit
'''

from contacts import Contact
from PySide import QtGui, QtCore
import AddContact

class ContactDialog(QtGui.QDialog, AddContact.Ui_AddContactForm):
    '''
    classdocs
    '''


    def __init__(self, contact=None, parent=None):
        '''
        Constructor
        '''
        super(ContactDialog, self).__init__(parent)
        
        
        self.setupUi(self)
        
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.showFullScreen()
        
        self.btnClose.clicked.connect(self.btnCloseClicked)
        self.btnBack.clicked.connect(self.btnCloseClicked)
        self.btnSave.clicked.connect(self.btnSaveClicked)
        
        if contact:     
            self.edtName.setText(contact.name)
            self.edtNumber.setText(contact.number)
            
        
        
    def btnCloseClicked(self):
        self.retValue = None
        self.close()
        
    def btnSaveClicked(self):
        if self.edtNumber.text() == '':
            self.retValue = None
        else:
            self.retValue = Contact(name = self.edtName.text(), number = self.edtNumber.text())
        self.close()
        
    def getContact(self):
        return self.retValue